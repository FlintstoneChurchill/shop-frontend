import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import ProductForm from "../../components/ProductForm/ProductForm";
import {createProduct as onProductCreated} from "../../store/actions/productsActions";
import {fetchCategories} from "../../store/actions/categoriesActions";

const NewProduct = () => {
  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories.categories);

  useEffect(() => {
    dispatch(fetchCategories());
  }, []);

  const createProduct = productData => {
    dispatch(onProductCreated(productData));
  };

  return (
    <>
      <h1>New product</h1>
      <ProductForm
        onSubmit={createProduct}
        categories={categories}
      />
    </>
  );
};

export default NewProduct;