import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import Products from "./containers/Products/Products";
import NewProduct from "./containers/NewProduct/NewProduct";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo} />;
};

const Routes = ({user}) => {
  return (
    <Switch>
      <Route path="/" exact component={Products}/>
      <ProtectedRoute
        path="/products/new"
        exact
        component={NewProduct}
        isAllowed={user && user.role === "admin"}
        redirectTo="/login"
      />
      <ProtectedRoute
        path="/register"
        exact
        component={Register}
        isAllowed={!user}
        redirectTo="/"
      />
      <ProtectedRoute
        path="/login"
        exact
        component={Login}
        isAllowed={!user}
        redirectTo="/"
      />
    </Switch>
  );
};

export default Routes;