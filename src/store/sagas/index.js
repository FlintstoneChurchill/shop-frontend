import {takeEvery} from "redux-saga/effects";
import {facebookLoginSaga, loginUserSaga, logoutUserSaga, registerUserSaga} from "./users";
import {FACEBOOK_LOGIN, LOGIN_USER, LOGOUT_USER, REGISTER_USER} from "../actionTypes";

export function* rootSaga() {
  yield takeEvery(REGISTER_USER, registerUserSaga);
  yield takeEvery(LOGIN_USER, loginUserSaga);
  yield takeEvery(LOGOUT_USER, logoutUserSaga);
  yield takeEvery(FACEBOOK_LOGIN, facebookLoginSaga);
}