import productsReducer from "./productsReducer";
import { fetchProductsSuccess, initialState } from '../actions/productsActions';

test("get products success", () => {
  const action = fetchProductsSuccess([
    {title: "test product", price: "200"}
  ]);

  const newState = productsReducer(initialState, action);
  expect(newState.products.length).toBe(1);
});